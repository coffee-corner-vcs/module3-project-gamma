import { NavLink } from "react-router-dom";


const Nav = () => {
  return (
    <nav className="navbar text-secondary bg-[#967259]">
      <div className="flex-1">
        <NavLink className="btn btn-ghost normal-case text-xl" to="/">
          Coffee Corner
        </NavLink>
      </div>
      <div className="flex-none">
        <ul className="flex">
          <li>
            <NavLink className="btn btn-ghost normal-case" to="/profile">
              My profile
            </NavLink>
          </li>
          <li>
            <NavLink className="btn btn-ghost normal-case" to="/reviews">
              Reviews
            </NavLink>
          </li>
          <li>
            <NavLink className="btn btn-ghost normal-case" to="/create-review">
              Create Review
            </NavLink>
          </li>
          <li>
            <NavLink className="btn btn-ghost normal-case" to="/art">
              Art
            </NavLink>
          </li>
          <li>
            <NavLink className="btn btn-ghost normal-case" to="/art-form">
              Create Art
            </NavLink>
          </li>
          <li>
            <NavLink className="btn btn-ghost normal-case" to="/recipes">
              Recipes
            </NavLink>
          </li>
          <li>
            <NavLink className="btn btn-ghost normal-case" to="/create-recipe">
              Create Recipe
            </NavLink>
          </li>
        </ul>
      </div>
    </nav>
  );
}

export default Nav;
