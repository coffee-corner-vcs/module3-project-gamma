import React, { useEffect, useState } from "react";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { useNavigate } from "react-router-dom";

export default function ArtForm() {
  const [image, setImage] = useState("");
  const [description, setDescription] = useState("");
  const [altText, setAltText] = useState("");
  const [userId, setUserId] = useState("");
  const [loading, setLoading] = useState(true);
  const [isTokenChecked, setIsTokenChecked] = useState(false);
  const { token, fetchWithToken } = useToken();
  const navigate = useNavigate();

  useEffect(() => {
    if (token && !isTokenChecked) {
      setLoading(false);
      setIsTokenChecked(true);
      fetchWithToken(`${process.env.REACT_APP_COFFEE_CORNER_API_HOST}/api/accounts/me`)
        .then((data) => {
          setUserId(data.id);
        })
        .catch((error) => {
          console.error(error);
          navigate("/login");
        });
    }
  }, [token, fetchWithToken, navigate, isTokenChecked]);
  if (loading) {
    return <div>Loading...</div>;
  }

  const handleChange = ({ target }, fn) => {
    return fn(target.value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {
      image: image,
      alt_text: altText,
      description: description,
      user_id: userId,
    };

    const url = `${process.env.REACT_APP_COFFEE_CORNER_API_HOST}/api/art`;

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`
      },
    };

    const response = await fetch(url, fetchConfig);
    if (!response.ok) {
      console.log("Could not create art post");
    }
    event.target.reset();
    navigate("/art");
  };

  return (
    <div className="min-h-screen flex items-center justify-center">
      <div className="card bg-primary rounded-lg shadow-lg w-full max-w-sm p-6">
        <h5 className="card-header text-black text-lg font-semibold mb-4">
          Art
          </h5>
      <form onSubmit={handleSubmit} className="art-form">
        <div className="text-secondary mb-4">
          <label htmlFor="image" className="form-label block text-sm font-semibold mb-2 text-secondary">
            Image
            <input
              name="image"
              type="text"
              value={image}
              placeholder="Input url"
              onChange={(e) => handleChange(e, setImage)}
              required
              className="input input-bordered bg-accent input-sm w-full max-w-xs border-gray-300 rounded-md shadow-sm focus:border-[#6699CC] focus:ring focus:ring-[#6699CC] focus:ring-opacity-50"
            />
          </label>
        </div>
        <div className="mb-4">
          <label htmlFor="altText" className="form-label block text-sm font-semibold mb-2 text-secondary">
            Alternative text
            <input
              name="altText"
              type="text"
              value={altText}
              placeholder="Alternative text"
              onChange={(e) => handleChange(e, setAltText)}
              required
              className="input input-bordered bg-accent input-sm w-full max-w-xs border-gray-300 rounded-md shadow-sm focus:border-[#6699CC] focus:ring focus:ring-[#6699CC] focus:ring-opacity-50"
            />
          </label>
        </div>
        <div className="mb-4">
          <label htmlFor="description" className="form-label block text-sm font-semibold mb-2 text-secondary">
            Description
            <input
              name="description"
              type="text"
              value={description}
              placeholder="Description"
              onChange={(e) => handleChange(e, setDescription)}
              required
              className="input bg-accent input-bordered input-sm w-full max-w-xs border-gray-300 rounded-md shadow-sm focus:border-[#6699CC] focus:ring focus:ring-[#6699CC] focus:ring-opacity-50"
            />
          </label>
        </div>
        <button className="btn btn-block bg-info text-black font-bold rounded hover:bg-secondary focus:accent-outline" type="submit">Submit</button>
      </form>
    </div>
    </div>
  );
}
