import React, { useEffect, useState } from 'react';
import useToken from '@galvanize-inc/jwtdown-for-react';
import { useNavigate } from 'react-router-dom';

function ProfilePage() {
    const [artVisible, setArtVisible] = useState(false);
    const [recipeVisible, setRecipeVisible] = useState(false);
    const [reviewVisible, setReviewVisible] = useState(false);
    const [createdArtVisible, setCreatedArtVisible] = useState(false);
    const [createdRecipeVisible, setCreatedRecipeVisible] = useState(false);
    const [createdReviewVisible, setCreatedReviewVisible] = useState(false);
    const [userData, SetUserData] = useState([]);
    const [likedArt, setLikedArt] = useState([]);
    const [likedReviews, setLikedReviews] = useState([]);
    const [likedRecipes, setLikedRecipes] = useState([]);
    const [createdArt, setCreatedArt] = useState([]);
    const [createdReviews, setCreatedReviews] = useState([]);
    const [createdRecipes, setCreatedRecipes] = useState([]);
    const [loading, setLoading] = useState(true);
    const { token, fetchWithToken } = useToken();
    const navigate = useNavigate();
    const [isTokenChecked, setIsTokenChecked] = useState(false);
    const [liked, setLiked] = useState(false);
    const [created, setCreated] = useState(false);
    const [users, setUsers] = useState([]);

    useEffect(() => {
        if (token && !isTokenChecked) {
          setIsTokenChecked(true);

              Promise.all([
                fetchWithToken(`${process.env.REACT_APP_COFFEE_CORNER_API_HOST}/likeart`),
                fetchWithToken(`${process.env.REACT_APP_COFFEE_CORNER_API_HOST}/api/accounts`),
              ])
                .then(([artData, usersData]) => {
                  setLikedArt(artData);
                  setUsers(usersData.data);

                })
                .catch((error) => {
                  console.error(error);
                  navigate("/login");
                });
        }
      }, [token, fetchWithToken, navigate, isTokenChecked, userData]);

    const findUsername = (userId) => {
        if (!Array.isArray(users)) {
          return "";
        }
        const user = users.find((user) => user.id === userId);
        return user ? user.username : "";
      };


    useEffect(() => {
        if (token && !isTokenChecked) {
            setLoading(false);
            setIsTokenChecked(true);
            fetchWithToken(`${process.env.REACT_APP_COFFEE_CORNER_API_HOST}/api/accounts/me`)
                .then((data) => {
                    SetUserData(data);
                })
                .catch((error) => {
                    console.error(error);
                    navigate("/login");
                });
        }
    }, [token, fetchWithToken, navigate, isTokenChecked]);


    useEffect(() => {
        if (token && !isTokenChecked) {
            setLoading(false);
            setIsTokenChecked(true);
            fetchWithToken(`${process.env.REACT_APP_COFFEE_CORNER_API_HOST}/likereview`)
                .then((data) => {
                    setLikedReviews(data);
                })
                .catch((error) => {
                    console.error(error);
                    navigate("/login");
                });
        }
    }, [token, fetchWithToken, navigate, isTokenChecked]);


    useEffect(() => {
        if (token && !isTokenChecked) {
            setLoading(false);
            setIsTokenChecked(true);
            fetchWithToken(`${process.env.REACT_APP_COFFEE_CORNER_API_HOST}/likerecipe`)
                .then((data) => {
                    setLikedRecipes(data);
                })
                .catch((error) => {
                    console.error(error);
                    navigate("/login");
                });
        }
    }, [token, fetchWithToken, navigate, isTokenChecked]);

    useEffect(() => {
        if (token && !isTokenChecked) {
            setLoading(false);
            setIsTokenChecked(true);
            fetchWithToken(`${process.env.REACT_APP_COFFEE_CORNER_API_HOST}/api/created/art`)
                .then((data) => {
                    setCreatedArt(data);
                })
                .catch((error) => {
                    console.error(error);
                    navigate("/login");
                });
        }
    }, [token, fetchWithToken, navigate, isTokenChecked]);

    useEffect(() => {
        if (token && !isTokenChecked) {
            setLoading(false);
            setIsTokenChecked(true);
            fetchWithToken(`${process.env.REACT_APP_COFFEE_CORNER_API_HOST}/api/created/reviews`)
                .then((data) => {
                    setCreatedReviews(data);
                })
                .catch((error) => {
                    console.error(error);
                    navigate("/login");
                });
        }
    }, [token, fetchWithToken, navigate, isTokenChecked]);

    useEffect(() => {
        if (token && !isTokenChecked) {
            setLoading(false);
            setIsTokenChecked(true);
            fetchWithToken(`${process.env.REACT_APP_COFFEE_CORNER_API_HOST}/api/created/recipes`)
                .then((data) => {
                    setCreatedRecipes(data);
                })
                .catch((error) => {
                    console.error(error);
                    navigate("/login");
                });
        }
    }, [token, fetchWithToken, navigate, isTokenChecked]);

    if (loading) {
        return <div>Loading...</div>;
    }

    const handleArtDelete = (id) => {
        fetch(
            `${process.env.REACT_APP_COFFEE_CORNER_API_HOST}/api/art/${id}`,
            {
                method: "DELETE",
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            }
        )
            .then((response) => {
                if (response.ok) {
                    fetchWithToken(`${process.env.REACT_APP_COFFEE_CORNER_API_HOST}/api/created/art`)
                        .then((data) => {
                            setCreatedArt(data);
                        })
                }
            })
            .catch((error) => {
                console.error(error);
            });
    };

    const handleRecipeDelete = (id) => {
        fetch(
            `${process.env.REACT_APP_COFFEE_CORNER_API_HOST}/recipes/${id}`,
            {
                method: "DELETE",
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            }
        )
            .then((response) => {
                if (response.ok) {
                    fetchWithToken(`${process.env.REACT_APP_COFFEE_CORNER_API_HOST}/api/created/recipes`)
                        .then((data) => {
                            setCreatedRecipes(data);
                        })
                }
            })
            .catch((error) => {
                console.error(error);
            });
    };

    const handleReviewDelete = (id) => {
        fetch(
            `${process.env.REACT_APP_COFFEE_CORNER_API_HOST}/reviews/${id}`,
            {
                method: "DELETE",
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            }
        )
            .then((response) => {
                if (response.ok) {
                    fetchWithToken(`${process.env.REACT_APP_COFFEE_CORNER_API_HOST}/api/created/reviews`)
                        .then((data) => {
                            setCreatedReviews(data);
                        })
                }
            })
            .catch((error) => {
                console.error(error);
            });
    };



    const changeHandler = (e) => {
        if (e.target.value === 'x') {
            setArtVisible(true);
        } else {
            setArtVisible(false);
        }
        if (e.target.value === 'y') {
            setRecipeVisible(true);
        } else {
            setRecipeVisible(false);
        }
        if (e.target.value === 'z') {
            setReviewVisible(true);
        } else {
            setReviewVisible(false);
        }
    };


    const createdChangeHandler = (e) => {
        if (e.target.value === 'a') {
            setCreatedArtVisible(true);
            setCreatedRecipeVisible(false);
            setCreatedReviewVisible(false);
        } else {
            setCreatedArtVisible(false);
        }
        if (e.target.value === 'b') {
            setCreatedRecipeVisible(true);
            setCreatedArtVisible(false);
            setCreatedReviewVisible(false);
        } else {
            setCreatedRecipeVisible(false);
        }
        if (e.target.value === 'c') {
            setCreatedReviewVisible(true);
            setCreatedArtVisible(false);
            setCreatedRecipeVisible(false);
        } else {
            setCreatedReviewVisible(false);
        }
    };

    const likedChangeHandler = (e) => {
        if (e.target.value) {
            setLiked(true);
            setCreated(false);
        }
        else {
            setLiked(false);
            setCreated(true);
        }
    }
    const CreatedChangeHandler = (e) => {
        if (e.target.value) {
            setLiked(false);
            setCreated(true);
        }
        else {
            setLiked(true);
            setCreated(false);
        }
    }

    function formatDate(dateString) {
        const options = { year: 'numeric', month: 'long', day: 'numeric' };
        return new Date(dateString).toLocaleDateString('en-US', options);
    }

    return (
        <div className="px-4 py-5 my-10 text-center">
        <div className="avatar">
            <div className="w-64 rounded-full flex-initial">
            <img className="profile-image" src={userData.image} alt="" />
            </div>
        </div>
        <div className="username">
            <div className="my-10 justify-center flex-initial font-black text-5xl font-serif text-secondary">
            <h1>{userData.username}</h1>
            </div>
        </div>
        <div className="place-content-center mb-5">
            <button value="liked" className=" border-box focus:bg-primary h-8 rounded-md font-serif justify-right mr-8 my-20 text-3xl text-secondary font-bold" onClick={likedChangeHandler}>
            Liked
            </button>
            <button value="created" className="border-box focus:bg-primary h-8 rounded-md font-serif justify-left ml-8 my-20 text-3xl text-secondary font-bold" onClick={CreatedChangeHandler}>
            Created
            </button>
        </div>
        {liked ? (
            <div>
            <div className="place-content-center">
            <button value="x" className="border-box focus:bg-primary rounded-md mb-12 font-serif justify-right mr-20  text-2xl text-secondary font-bold" onClick={changeHandler}>
            Art
            </button>
            <button value="y" className="border-box focus:bg-primary rounded-md mb-12 font-serif text-2xl text-secondary font-bold" onClick={changeHandler}>
            Recipe
            </button>
            <button value="z" className="border-box focus:bg-primary rounded-md mb-12 font-serif justify-left ml-20 text-2xl text-secondary font-bold" onClick={changeHandler}>
            Review
            </button>
            </div>
            <div className="flex center">
                {artVisible ? (
                <div className="m-auto max-w-xl text-secondary">
                {likedArt.map((art) => (
                <div key={art.id} className="card max-w-xl bg-base-500 shadow-xl m-7 bg-primary">
                    <figure>
                    <img src={art.image} alt={art.alt_text} />
                    </figure>
                    <div className="card-body shadow-inner bg-primary">
                    <div className="flex items-center">
                        <div className="avatar">
                        <div className="w-10 rounded-full text-secondary">
                            <img
                            src={art.profile_image}
                            alt={`user ${art.username}`}
                            />
                        </div>
                        </div>
                        <p className="textarea-sm pl-4 text-lg flex item-left text-secondary font-bold">{findUsername(art.user_id)}</p>
                    </div>
                    <div className="my-3 border-t-2 text-left border-secondary bg-primary">
                    <p className="text-secondary justify-left mb-10 my-4">{art.description}</p>
                    </div>
                    </div>
                    </div>
                    ))}
                    </div>
                ) : null}

                {recipeVisible ? (
                    <div className="m-auto max-w-xl text-secondary">
                    {likedRecipes.map((recipe) => (
                    <div key={recipe.id} className="card max-w-xl bg-base-500 shadow-xl m-7 bg-primary">
                        <figure>
                        <img src={recipe.image} alt={recipe.alt_text} />
                        </figure>
                        <div className="card-body shadow-inner bg-primary">
                        <div className="flex items-center">
                            <div className="avatar">
                            <div className="w-10 rounded-full text-secondary">
                                <img
                                src={recipe.profile_image}
                                alt={`user ${recipe.username}`}
                                />
                            </div>
                            </div>
                            <p className="textarea-sm pl-4 flex item-left text-lg text-secondary font-bold">{findUsername(recipe.user_id)}</p>
                        </div>
                        <div className="my-3 border-t-2 text-left border-secondary bg-primary">
                        <p className="text-md text-secondary my-4 text-lg font-heavy">{recipe.description}</p>
                        <p className="text-md text-secondary mt-10"><strong>Ingredients:</strong> {recipe.ingredients}</p>
                        <p className="text-md text-secondary mb-2"><strong>Roast:</strong> {recipe.roast}</p>
                        </div>
                        </div>
                        </div>
                        ))}
                        </div>
                ) : null}
                {reviewVisible ? (
                <div className="m-auto max-w-xl text-secondary">
                    {likedReviews.map((review) => (
                    <div key={review.id} className="card max-w-xl bg-base-500 shadow-xl m-7 bg-primary">
                        <figure>
                        <img src={review.image} alt={review.alt_text} />
                        </figure>
                        <div className="card-body shadow-inner bg-primary">
                        <div className="flex items-center">
                            <div className="avatar">
                            <div className="w-10 rounded-full text-secondary">
                                <img
                                src={review.profile_image}
                                alt={`user ${review.username}`}
                                />
                            </div>
                            </div>
                            <p className="textarea-sm pl-4 flex item-left text-lg text-secondary font-bold">{findUsername(review.user_id)}</p>
                        </div>
                        <div className="my-3 border-t-2 text-left border-secondary bg-primary">
                        <p className="text-md text-secondary mb-10 my-4 text-lg font-heavy">{review.description}</p>
                        <p className="text-md text-secondary mt-10"><strong>Stars:</strong>: {review.rating}</p>
                        <p className="text-md text-secondary mb-2"><strong>Created:</strong> {formatDate(review.date_created)}</p>
                        </div>
                        </div>
                        </div>
                        ))}
                        </div>
                ) : null}
                </div>
                </div>
                 ) : null }
                    {created ? (
                    <div>
                    <div className="place-content-center">
                    <button value="a" className="border-box focus:bg-primary rounded-md mb-12 font-serif justify-right mr-20  text-2xl text-secondary font-bold" onClick={createdChangeHandler}>
                    Art
                    </button>
                    <button value="b" className="border-box focus:bg-primary rounded-md mb-12 font-serif text-2xl text-secondary font-bold" onClick={createdChangeHandler}>
                    Recipe
                    </button>
                    <button value="c" className="border-box focus:bg-primary rounded-md mb-12 font-serif justify-left ml-20 text-2xl text-secondary font-bold" onClick={createdChangeHandler}>
                    Review
                    </button>
                    </div>
                    <div className="flex center"></div>
                    {createdArtVisible ? (
                        <div className="m-auto max-w-xl text-secondary">
                        {createdArt.map((art) => (
                        <div key={art.id} className="card max-w-xl bg-base-500 shadow-xl m-7 bg-primary">
                            <figure>
                            <img src={art.image} alt={art.alt_text} />
                            </figure>
                            <div className="card-body shadow-inner bg-primary">
                            <div className="flex items-center">
                                <div className="avatar">
                                <div className="w-10 rounded-full text-secondary">
                                    <img
                                    src={art.profile_image}
                                    alt={`user ${art.username}`}
                                    />
                                </div>
                                </div>
                                <p className="textarea-sm pl-4 text-lg flex item-left text-secondary font-bold">{findUsername(art.user_id)}</p>
                            </div>
                            <div className="my-3 border-t-2 border-secondary bg-primary">
                            <p className="text-secondary text-left my-4">{art.description}</p>
                            <button className="delete-art text-right ml-80" id="delete-art" onClick={() => { handleArtDelete(art.id) }}>Delete</button>
                            </div>
                            </div>
                            </div>
                            ))}
                            </div>
                    ) : null}
                    {createdRecipeVisible ? (
                    <div className="m-auto max-w-xl text-secondary">
                    {createdRecipes.map((recipe) => (
                    <div key={recipe.id} className="card max-w-xl bg-base-500 shadow-xl m-7 bg-primary">
                        <figure>
                        <img src={recipe.image} alt={recipe.alt_text} />
                        </figure>
                        <div className="card-body shadow-inner bg-primary">
                        <div className="flex items-center">
                            <div className="avatar">
                            <div className="w-10 rounded-full text-secondary">
                                <img
                                src={recipe.profile_image}
                                alt={`user ${recipe.username}`}
                                />
                            </div>
                            </div>
                            <p className="textarea-sm pl-4 text-lg flex item-left text-secondary font-bold">{findUsername(recipe.user_id)}</p>
                        </div>
                        <div className="my-3 border-t-2 border-secondary bg-primary">
                        <p className="text-md text-secondary text-left my-4 text-lg font-heavy">{recipe.description}</p>
                        <p className="text-md text-secondary text-left mt-10"><strong>Ingredients:</strong> {recipe.ingredients}</p>
                        <p className="text-md text-secondary text-left mb-2"><strong>Roast:</strong> {recipe.roast}</p>
                        <button className="justify-right text-right ml-80" id="delete-recipe" onClick={() => { handleRecipeDelete(recipe.id) }}>Delete</button>
                        </div>
                        </div>
                        </div>
                    ))}
                    </div>
                    ) : null}
                    {createdReviewVisible ? (
                        <div id="showCreatedReviews" className="m-auto max-w-xl text-secondary">
                            {createdReviews.map((review) => (
                                <div key={review.id} className="card max-w-xl bg-base-500 shadow-xl m-7 bg-primary">
                        <figure>
                        <img src={review.image} alt={review.alt_text} />
                        </figure>
                        <div className="card-body shadow-inner bg-primary">
                        <div className="flex items-center">
                            <div className="avatar">
                            <div className="w-10 rounded-full text-secondary">
                                <img
                                src={review.profile_image}
                                alt={`user ${review.username}`}
                                />
                            </div>
                            </div>
                            <p className="textarea-sm pl-4 flex item-left text-lg text-secondary font-bold">{findUsername(review.user_id)}</p>
                        </div>
                                    <div className="my-3 border-t-2 border-secondary bg-primary">
                                    <p className="text-md text-secondary text-left my-4 text-lg font-heavy">{review.description}</p>
                                    <p className="text-md text-secondary text-left mt-10 mb-2"><strong>Stars:</strong> {review.rating}</p>
                                    <p className="text-md text-secondary text-left mb-2"><strong>Created:</strong> {formatDate(review.date_created)}</p>
                                    <button className="delete-review justify-right text-right ml-80" id="delete-review" onClick={() => { handleReviewDelete(review.id) }}>Delete</button>
                                </div>
                                </div>
                                </div>
                            ))}
                            </div>
                    ) : null }
                </div>
                ) : null}
                </div>
    );

}

export default ProfilePage;
