import React, { useState, useEffect } from "react";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { useNavigate } from "react-router-dom";

function ReviewForm() {
  const [image, setImage] = useState("");
  const [altText, setAltText] = useState("");
  const [description, setDescription] = useState("");
  const [rating, setRating] = useState("");
  const [dateCreated, setDateCreated] = useState("");
  const { token, fetchWithToken } = useToken();
  const navigate = useNavigate();
  const [loading, setLoading] = useState(true);
  const [data, setUserData] = useState("");
  const [isTokenChecked, setIsTokenChecked] = useState(false);

  useEffect(() => {
    if (token && !isTokenChecked) {
      setLoading(false);
      setIsTokenChecked(true);
      fetchWithToken(`${process.env.REACT_APP_COFFEE_CORNER_API_HOST}/api/accounts/me`)
        .then((data) => {
          setUserData(data);
        })
        .catch((error) => {
          console.error(error);
          navigate("/login");
        });
    }
  }, [token, fetchWithToken, navigate, isTokenChecked]);

  if (loading) {
    return <div>Loading...</div>;
  }


  const handleSubmit = (e) => {
    e.preventDefault();
    fetch(`${process.env.REACT_APP_COFFEE_CORNER_API_HOST}/reviews`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`
      },
      body: JSON.stringify({
        image,
        alt_text: altText,
        description,
        rating,
        date_created: dateCreated,
        user_id: data.id
      }),
    })
      .then((response) => {
        if (!response.ok) {
          throw new Error("HTTP error " + response.status);
        }
        e.target.reset();
        navigate("/reviews");
      })
      .catch((error) => console.error(error));
  };

  return (
    <div className="min-h-screen flex items-center justify-center">
    <div className="card bg-primary rounded-lg shadow-lg w-full max-w-sm p-6">
      <h5 className="card-header text-black text-lg font-semibold mb-4">
        Review
      </h5>
        <form onSubmit={handleSubmit}>
          <div className="mb-4 text-info">
            <label className="form-label block text-sm font-semibold mb-2 text-secondary">Image URL:</label>
            <input
              type="text"
              value={image}
              onChange={(e) => setImage(e.target.value)}
              required
              className="input input-bordered bg-accent input-sm w-full max-w-xs border-gray-300 rounded-md shadow-sm focus:border-[#6699CC] focus:ring focus:ring-[#6699CC] focus:ring-opacity-50"
            />
          </div>
          <div className="mb-4 text-info">
            <label className="form-label block text-sm font-semibold mb-2 text-secondary">Alt Text:</label>
            <input
              type="text"
              value={altText}
              onChange={(e) => setAltText(e.target.value)}
              required
              className="input input-bordered bg-accent input-sm w-full max-w-xs border-gray-300 rounded-md shadow-sm focus:border-[#6699CC] focus:ring focus:ring-[#6699CC] focus:ring-opacity-50"
            />
          </div>
          <div className="mb-4 text-info">
            <label className="form-label block text-sm font-semibold mb-2 text-secondary">Description:</label>
            <textarea
              value={description}
              onChange={(e) => setDescription(e.target.value)}
              required
              className="input input-bordered bg-accent input-sm w-full max-w-xs border-gray-300 rounded-md shadow-sm focus:border-[#6699CC] focus:ring focus:ring-[#6699CC] focus:ring-opacity-50"
            />
          </div>
          <div className="mb-4 text-info">
            <label className="form-label block text-sm font-semibold mb-2 text-secondary">Rating:</label>
            <input
              type="text"
              value={rating}
              placeholder="1-5"
              onChange={(e) => setRating(e.target.value)}
              required
              className="input input-bordered bg-accent input-sm w-full max-w-xs border-gray-300 rounded-md shadow-sm focus:border-[#6699CC] focus:ring focus:ring-[#6699CC] focus:ring-opacity-50"
            />
          </div>
          <div className="mb-4 text-info">
            <label className="form-label block text-sm font-semibold mb-2 text-secondary">Date Created:</label>
            <input
              type="text"
              value={dateCreated}
              placeholder="mm/dd/yyyy"
              onChange={(e) => setDateCreated(e.target.value)}
              required
              className="input input-bordered bg-accent input-sm w-full max-w-xs border-gray-300 rounded-md shadow-sm focus:border-[#6699CC] focus:ring focus:ring-[#6699CC] focus:ring-opacity-50"
            />
          </div>
          <div>
            <button type="submit" className="btn bg-info btn-block text-black font-bold rounded hover:bg-secondary focus:accent-outline">
              Submit
            </button>
          </div>
        </form>
      </div>
    </div>
  );

}

export default ReviewForm;
