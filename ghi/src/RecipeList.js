import { useState, useEffect } from "react";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { useNavigate } from "react-router-dom";

function RecipeList() {
  const [recipes, setRecipes] = useState([]);
  const [loading, setLoading] = useState(true);
  const { token, fetchWithToken } = useToken();
  const navigate = useNavigate();
  const [isTokenChecked, setIsTokenChecked] = useState(false);
  const [commentText, setCommentText] = useState("");
  const [showAllComments, setShowAllComments] = useState(false);
  const [userId, setUserId] = useState(null);
  const [likedRecipes, setLikedRecipes] = useState([]);
  const [likes, setLikes] = useState([]);
  const [users, setUsers] = useState([]);

  useEffect(() => {
    if (token && !isTokenChecked) {
      setIsTokenChecked(true);

      fetchWithToken(
        `${process.env.REACT_APP_COFFEE_CORNER_API_HOST}/api/accounts/me`
      )
        .then((data) => {
          setUserId(data.id);
          localStorage.setItem("user_id", data.id);

          Promise.all([
            fetchWithToken(
              `${process.env.REACT_APP_COFFEE_CORNER_API_HOST}/recipes`
            ),
            fetchWithToken(
              `${process.env.REACT_APP_COFFEE_CORNER_API_HOST}/api/accounts`
            ),
            fetchWithToken(
              `${process.env.REACT_APP_COFFEE_CORNER_API_HOST}/likerecipe`
            ),
          ])
            .then(([recipesData, usersData, likesData]) => {
              setRecipes(recipesData);
              setUsers(usersData.data);

              const likedRecipeIds = likesData
                .filter((like) => like.user_id === data.id)
                .map((like) => like.recipe_id);
              setLikedRecipes(likedRecipeIds);

              setLikes(likesData);
              setLoading(false);
            })
            .catch((error) => {
              console.error(error);
              navigate("/login");
            });
        })
        .catch((error) => {
          console.error(error);
          navigate("/login");
        });
    }
  }, [token, fetchWithToken, navigate, isTokenChecked, userId]);

  //Likes Code
  function toggleLikeRecipe(recipeId) {
    const userId = Number(localStorage.getItem("user_id"));
    const isLiked = likes.some(
      (like) =>
        Number(like.user_id) === userId && Number(like.recipe_id) === recipeId
    );
    const likeId = likes.find(
      (like) =>
        Number(like.user_id) === userId && Number(like.recipe_id) === recipeId
    )?.id;

    const requestOptions = isLiked
      ? {
          method: "DELETE",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
          },
        }
      : {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
          },
          body: JSON.stringify({
            user_id: userId,
            recipe_id: recipeId,
          }),
        };

    fetch(
      `${process.env.REACT_APP_COFFEE_CORNER_API_HOST}/likerecipe${
        isLiked ? `/${likeId}` : ""
      }`,
      requestOptions
    )
      .then((response) => {
        if (!response.ok) throw new Error(response.status);
        else return response.json();
      })
      .then((data) => {
        if (isLiked) {
          setLikes((likes) =>
            likes.filter(
              (like) =>
                !(like.user_id === userId && like.recipe_id === recipeId)
            )
          );
          setLikedRecipes((likedRecipes) =>
            likedRecipes.filter((id) => id !== recipeId)
          );
        } else {
          setLikes((likes) => [
            ...likes,
            { ...data, user_id: userId, recipe_id: recipeId },
          ]);
          setLikedRecipes((likedRecipes) => [...likedRecipes, recipeId]);
        }
      })
      .catch((error) => {
        console.error("Error:", error);
        console.error("Server Response:", error.response);
      });
  }

  const findUsername = (userId) => {
    if (!Array.isArray(users)) {
      return "User not found";
    }
    const user = users.find((user) => user.id === userId);
    return user ? user.username : "User not found";
  };

  //End Likes Code

  //Comments code

  const handleCommentChange = (event, recipeId) => {
    setCommentText((prevState) => ({
      ...prevState,
      [recipeId]: event.target.value,
    }));
  };

  const handleAddComment = (recipeId) => {
    const userId = localStorage.getItem("user_id");
    const comment = {
      user_id: parseInt(userId),
      recipe_id: recipeId,
      comment_text: commentText[recipeId],
    };
    fetch(`${process.env.REACT_APP_COFFEE_CORNER_API_HOST}/commentrecipe`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(comment),
    })
      .then((response) => {
        if (!response.ok) {
          throw new Error("Network didn't like that");
        }
        return response.json();
      })
      .then(() => {
        setCommentText((prevState) => ({
          ...prevState,
          [recipeId]: "",
        }));
        fetchWithToken(
          `${process.env.REACT_APP_COFFEE_CORNER_API_HOST}/recipes`
        )
          .then((data) => {
            setRecipes(data);
          })
          .catch((error) => {
            console.error(error);
            navigate("/login");
          });
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const toggleShowAllComments = (recipeId) => {
    setShowAllComments((prevState) => ({
      ...prevState,
      [recipeId]: !prevState[recipeId],
    }));
  };

  //end comments code

  if (loading) {
    return <div>Loading...</div>;
  }

  return (
    <div className="m-auto max-w-xl text-secondary">
      {recipes.map((recipe) => {
        const isLiked = likedRecipes.includes(Number(recipe.id));
        return (
          <div
            key={recipe.id}
            className="card max-w-xl bg-base-500 shadow-xl m-7 bg-primary"
          >
            <strong className="text-center">{recipe.title}</strong>
            <figure>
              <img src={recipe.image} alt={recipe.alt_text} />
            </figure>
            <div className="card-body shadow-inner bg-primary">
              <div className="flex items-center">
                <div className="avatar">
                  <div className="w-10 rounded-full text-secondary">
                    <img
                      src={recipe.profile_image}
                      alt={`user ${recipe.username}`}
                    />
                  </div>
                  </div>
                  <p className="textarea-sm pl-4 text-lg text-secondary font-bold">
                {findUsername(recipe.user_id)}
              </p>
              <div className="inline-flex items-center justify-center w-7 h-7 ml-8 pr-7 text-secondary" onClick={() => toggleLikeRecipe(recipe.id)}>
                {isLiked ? "unlike" :"like"}
            </div>
            </div>
            <div className="my-3 border-t-2 border-secondary bg-primary">
              <p className="text-secondary justify-left mb-10 my-4">{recipe.description}</p>
              <p className="text-secondary justify-left">
                <strong>Ingredients: </strong>
                {recipe.ingredients}
              </p>
              <p>
                <strong>Roast: </strong>
                {recipe.roast}
              </p>
            </div>
            <div className="comment-section text-secondary font-bold pl-5 bg-primary">
              <h3>Comments</h3>
              <div className="comment-list bg-primary font-medium">
                {recipe.comments
                  .slice(
                    0,
                    showAllComments[recipe.id] ? recipe.comments.length : 2
                  )
                  .map(
                    (comment, index) =>
                      comment && (
                        <div key={index} className="comment">
                          <p>
                            <strong>{comment.username}: </strong>
                            {comment.comment_text}
                          </p>
                        </div>
                      )
                  )}
                {recipe.comments.length > 2 && (
                  <button onClick={() => toggleShowAllComments(recipe.id)}>
                    {showAllComments[recipe.id] ? "Show Less" : "Show More"}
                  </button>
                )}
              </div>
              <div className="add-comment bg-primary">
                <input
                  className="rounded bg-accent m-5"
                  type="text"
                  value={commentText[recipe.id] || ""}
                  onChange={(event) => handleCommentChange(event, recipe.id)}
                  placeholder="Comment:"
                />
                <button onClick={() => handleAddComment(recipe.id)}>
                  Add Comment
                </button>
              </div>
            </div>
          </div>
          </div>
        );
      })}
    </div>
  );
}

export default RecipeList;
