import { useState, useEffect } from 'react';
import useToken from '@galvanize-inc/jwtdown-for-react';
import { useNavigate } from 'react-router-dom';


function ReviewList() {
    const [reviews, setReviews] = useState([]);
    const [users, setUsers] = useState([]);
    const [likes, setLikes] = useState([]);
    const [loading, setLoading] = useState(true);
    const { token, fetchWithToken } = useToken();
    const navigate = useNavigate();
    const [isTokenChecked, setIsTokenChecked] = useState(false);
    const [userId, setUserId] = useState(null);
    const [likedReviews, setLikedReviews] = useState([]);

    useEffect(() => {
        if (token && !isTokenChecked) {
            setIsTokenChecked(true);

            fetchWithToken(`${process.env.REACT_APP_COFFEE_CORNER_API_HOST}/api/accounts/me`)
                .then((data) => {
                    setUserId(data.id);
                    localStorage.setItem("user_Id", data.id);

                    Promise.all([
                        fetchWithToken(`${process.env.REACT_APP_COFFEE_CORNER_API_HOST}/reviews`),
                        fetchWithToken(`${process.env.REACT_APP_COFFEE_CORNER_API_HOST}/api/accounts`),
                        fetchWithToken(`${process.env.REACT_APP_COFFEE_CORNER_API_HOST}/likereview`)
                    ])
                    .then(([reviewsData, usersData, likesData]) => {
                        setReviews(reviewsData);
                        setUsers(usersData.data);

                        const likedReviewIds = likesData.filter(like => like.user_id === data.id).map(like => like.review_id);
                        setLikedReviews(likedReviewIds);

                        setLikes(likesData);
                        setLoading(false);
                    })
                    .catch((error) => {
                        console.error(error);
                        navigate("/login");
                    });
                })
                .catch((error) => {
                    console.error(error);
                    navigate("/login");
                });
        }
    }, [token, fetchWithToken, navigate, isTokenChecked, userId]);


    if (loading) {
        return <div>Loading...</div>;
    }

    function toggleLikeReview(reviewId) {
        const userId = Number(localStorage.getItem("user_Id"));
        const isLiked = likes.some(like => Number(like.user_id) === userId && Number(like.review_id) === reviewId);
        const likeId = likes.find(like => Number(like.user_id) === userId && Number(like.review_id) === reviewId)?.id;

        const requestOptions = isLiked
        ? {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
        }
        : {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({
                user_id: userId,
                review_id: reviewId,
            }),
        };

        fetch(`${process.env.REACT_APP_COFFEE_CORNER_API_HOST}/likereview${isLiked ? `/${likeId}` : ''}`, requestOptions)
        .then((response) => {
            if (!response.ok) throw new Error(response.status);
            else return response.json();
        })
        .then((data) => {
            if (isLiked) {
                setLikes(likes => likes.filter(like => !(like.user_id === userId && like.review_id === reviewId)));
                setLikedReviews(likedReviews => likedReviews.filter(id => id !== reviewId));
            } else {
                setLikes(likes => [...likes, { ...data, user_id: userId, review_id: reviewId }]);
                setLikedReviews(likedReviews => [...likedReviews, reviewId]);
            }
        })
        .catch((error) => {
            console.error('Error:', error);
            console.error('Server Response:', error.response);
        });
    }

    const findUsername = (userId) => {
        if (!Array.isArray(users)) {
            return 'User not found';
        }
        const user = users.find(user => user.id === userId);
        return user ? user.username : 'User not found';
    };

    function formatDate(dateString) {
        const options = { year: 'numeric', month: 'long', day: 'numeric' };
        return new Date(dateString).toLocaleDateString('en-US', options);
    }

    return (
        <div className="m-auto max-w-xl text-secondary">
            {reviews.map((review) => {
                const isLiked = likedReviews.includes(Number(review.id));
                return (
                    <div key={review.id} className="card max-w-xl bg-base-500 shadow-xl m-7 bg-primary">
                        <figure>
                            <img
                                src={review.image}
                                alt={review.alt_text}
                            />
                        </figure>
                        <div className="card-body shadow-inner bg-primary">
                        <div className="flex items-center">
                            <div className="avatar">
                            <div className="w-10 rounded-full text-secondary">
                                <img
                                src={review.profile_image}
                                alt={`user ${review.username}`}
                                />
                            </div>
                            </div>
                            <p className="textarea-sm pl-4 text-lg text-secondary font-bold">
                            {findUsername(review.user_id)}
                            </p>
                            <div className="inline-flex items-center justify-center w-7 h-7 ml-8 pr-7 text-secondary" onClick={() => toggleLikeReview(review.id)}>
                                {isLiked ? "unlike" :"like"}
                            </div>
                            </div>

                            <div className="my-3 border-t-2 border-secondary bg-primary">
                            <p className="text-secondary justify-left mb-10 my-4">{review.description}</p>
                            <p className="text-secondary justify-left"><strong>Rating:</strong> {review.rating}</p>
                            <p><strong>Created:</strong> {formatDate(review.date_created)}</p>
                            </div>
                        </div>
                    </div>
                );
            })}
        </div>

    );

}

export default ReviewList;
