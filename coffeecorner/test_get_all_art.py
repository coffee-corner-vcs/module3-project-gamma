from main import app
from fastapi.testclient import TestClient
from queries.art import ArtRepository
from authenticator import authenticator

client = TestClient(app)


class FakeArtRepository:
    def get_all_art(self):
        result = [
            {
                "id": 1,
                "user_id": 4,
                "image": "https://tinyurl.com/caffeinatedcoffee",
                "description": "This is a description.",
                "alt_text": "This is alternative text.",
                "created_date": "2023-05-31T23:55:15.394515",
                "username": "coffeedrinker",
                "profile_image": "https://placehold.co/400",
            },
            {
                "id": 2,
                "user_id": 4,
                "image": "https://tinyurl.com/caffeinatedcoffee",
                "description": "This is a description.",
                "alt_text": "This is alternative text.",
                "created_date": "2023-05-31T23:55:15.394515",
                "username": "letmehavealatte",
                "profile_image": "https://placehold.co/400",
            },
        ]
        return result


def fake_get_current_account_data():
    pass


def test_get_art():
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data
    app.dependency_overrides[ArtRepository] = FakeArtRepository
    data = [
        {
            "id": 1,
            "user_id": 4,
            "image": "https://tinyurl.com/caffeinatedcoffee",
            "description": "This is a description.",
            "alt_text": "This is alternative text.",
            "created_date": "2023-05-31T23:55:15.394515",
            "username": "coffeedrinker",
            "profile_image": "https://placehold.co/400",
        },
        {
            "id": 2,
            "user_id": 4,
            "image": "https://tinyurl.com/caffeinatedcoffee",
            "description": "This is a description.",
            "alt_text": "This is alternative text.",
            "created_date": "2023-05-31T23:55:15.394515",
            "username": "letmehavealatte",
            "profile_image": "https://placehold.co/400",
        },
    ]
    expected = [
        {
            "id": 1,
            "user_id": 4,
            "image": "https://tinyurl.com/caffeinatedcoffee",
            "description": "This is a description.",
            "alt_text": "This is alternative text.",
            "created_date": "2023-05-31T23:55:15.394515",
            "username": "coffeedrinker",
            "profile_image": "https://placehold.co/400",
        },
        {
            "id": 2,
            "user_id": 4,
            "image": "https://tinyurl.com/caffeinatedcoffee",
            "description": "This is a description.",
            "alt_text": "This is alternative text.",
            "created_date": "2023-05-31T23:55:15.394515",
            "username": "letmehavealatte",
            "profile_image": "https://placehold.co/400",
        },
    ]
    response = client.get("/api/art", json=data)

    app.dependency_overrides = {}

    assert response.status_code == 200
    assert response.json() == expected
