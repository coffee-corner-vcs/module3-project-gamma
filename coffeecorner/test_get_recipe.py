from main import app
from fastapi.testclient import TestClient
from authenticator import authenticator
from queries.recipes import RecipeRepository

client = TestClient(app)


class FakeRecipeRepo:
    def get_one(self, recipe_id: 1):
        result = {
            "title": "espresso",
            "id": 1,
            "image": "https://tinyurl.com/4mykura5",
            "alt_text": "Coffee Image",
            "description": "Good coffee.",
            "ingredients": "bleck coffee, honey",
            "roast": "Dark",
            "user_id": 1,
        }
        return result


def fake_account():
    return {
        "id": 1,
        "image": "https://tinyurl.com/4mykura5",
        "username": "zach",
        "email": "zach@gmail.com",
    }


def test_get_one_recipe():
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_account
    app.dependency_overrides[RecipeRepository] = FakeRecipeRepo

    response = client.get("/recipes/1")

    app.dependency_overrides = {}

    assert response.status_code == 200
    assert response.json() == {
        "title": "espresso",
        "id": 1,
        "image": "https://tinyurl.com/4mykura5",
        "alt_text": "Coffee Image",
        "description": "Good coffee.",
        "ingredients": "bleck coffee, honey",
        "roast": "Dark",
        "user_id": 1,
    }
