from fastapi import APIRouter, Depends
from queries.reviews import ReviewIn, ReviewRepository
from authenticator import authenticator


router = APIRouter()


@router.post("/reviews")
def create_review(
    review: ReviewIn,
    repo: ReviewRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.create(review)


@router.get("/reviews")
def get_reviews(
    repo: ReviewRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.get_all()


@router.get("/reviews/{review_id}")
def get_review(
    review_id: int,
    repo: ReviewRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.get_one(review_id)


@router.put("/reviews/{review_id}")
def update_review(
    review_id: int,
    review: ReviewIn,
    repo: ReviewRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.update(review_id, review)


@router.delete("/reviews/{review_id}")
def delete_review(
    review_id: int,
    repo: ReviewRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.delete(review_id)


@router.get("/api/created/reviews")
def get_created_reviews(
    repo: ReviewRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.created_reviews(account_data)
