from fastapi import APIRouter, Depends, Response
from queries.art import ArtIn, ArtRepository
from authenticator import authenticator

router = APIRouter()


@router.post("/api/art")
def create_new_art(
    art: ArtIn,
    repo: ArtRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data)
):
    return repo.create_art(art)


@router.get("/api/art")
def get_all_existing_art(
    repo: ArtRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data)
):
    return repo.get_all_art()


@router.get("/api/art/{art_id}")
def get_existing_art(
    art_id: int,
    response: Response,
    repo: ArtRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data)
):
    art = repo.get_art(art_id)
    if art is None:
        response.status_code = 404
        return {"message": "Art not found"}
    return repo.get_art(art_id)


@router.put("/api/art/{art_id}")
def update_existing_art(
    art_id: int,
    art: ArtIn,
    repo: ArtRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.update_art(art_id, art)


@router.delete("/api/art/{art_id}")
def delete_existing_art(
    art_id: int,
    repo: ArtRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.delete_art(art_id)


@router.get("/api/created/art")
def get_created_art(
    repo: ArtRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.created_art(account_data)
