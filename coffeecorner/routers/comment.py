from fastapi import APIRouter, Depends
from authenticator import authenticator
from queries.comment import (
    CommentIn,
    CommentRepository
)

router = APIRouter()


@router.post("/commentart")
def comment_art_post(
    comment: CommentIn,
    repo: CommentRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.artcomment(comment)


@router.post("/commentrecipe")
def comment_recipe_post(
    comment: CommentIn,
    repo: CommentRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.recipecomment(comment)


@router.post("/commentreview")
def comment_review_post(
    comment: CommentIn,
    repo: CommentRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.reviewcomment(comment)


@router.get("/commentart")
def get_art_comments(
    repo: CommentRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.getartcomments()


@router.get("/commentrecipe")
def get_recipe_comments(
    repo: CommentRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.getrecipecomments()


@router.get("/commentreview")
def get_review_comments(
    repo: CommentRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.getreviewcomments()


@router.delete("/commentart/{id}", response_model=bool)
def delete_art_comment(
    id: int,
    repo: CommentRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> bool:
    return repo.deleteartcomment(id)


@router.delete("/commentrecipe/{id}", response_model=bool)
def delete_recipe_comment(
    id: int,
    repo: CommentRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> bool:
    return repo.deleterecipecomment(id)


@router.delete("/commentreview/{id}", response_model=bool)
def delete_review_comment(
    id: int,
    repo: CommentRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> bool:
    return repo.deletereviewcomment(id)
