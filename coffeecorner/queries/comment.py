from pydantic import BaseModel
from queries.pool import pool
from typing import List, Optional, Union


class Error(BaseModel):
    message: str


class CommentIn(BaseModel):
    user_id: int
    art_id: Optional[int]
    review_id: Optional[int]
    recipe_id: Optional[int]
    comment_text: str


class CommentArtOut(BaseModel):
    id: int
    user_id: int
    art_id: int
    comment_text: str


class CommentRecipeOut(BaseModel):
    id: int
    user_id: int
    recipe_id: int
    comment_text: str


class CommentReviewOut(BaseModel):
    id: int
    user_id: int
    review_id: int
    comment_text: str


class CommentRepository:
    def deleteartcomment(self, id: int) -> Optional[CommentArtOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                            DELETE FROM comment
                            WHERE id = %s
                            """,
                        [id],
                    )
                    return True
        except Exception as e:
            print(e)
            return False

    def deleterecipecomment(self, id: int) -> Optional[CommentRecipeOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                            DELETE FROM comment
                            WHERE id = %s
                            """,
                        [id],
                    )
                    return True
        except Exception as e:
            print(e)
            return False

    def deletereviewcomment(self, id: int) -> Optional[CommentReviewOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                            DELETE FROM comment
                            WHERE id = %s
                            """,
                        [id],
                    )
                    return True
        except Exception as e:
            print(e)
            return False

    def getartcomments(self) -> Union[Error, List[CommentArtOut]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT comment.id,
                        comment.user_id, comment.art_id,
                        comment.comment_text
                        FROM comment
                        INNER JOIN art
                            ON (comment.art_id = art.id)
                        ORDER BY art_id DESC
                        """
                    )
                    result = []
                    for post in db:
                        comment = CommentArtOut(
                            id=post[0],
                            user_id=post[1],
                            art_id=post[2],
                            comment_text=post[3],
                        )
                        result.append(comment)
                    return result
        except Exception as e:
            print(e)
            return {"message": "Could not list comments"}

    def getrecipecomments(self) -> Union[Error, List[CommentRecipeOut]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT comment.id, comment.user_id,
                        comment.recipe_id, comment.comment_text
                        FROM comment
                        INNER JOIN recipe
                            ON (comment.recipe_id = recipe.id)
                        ORDER BY recipe_id DESC
                        """
                    )
                    result = []
                    for post in db:
                        comment = CommentRecipeOut(
                            id=post[0],
                            user_id=post[1],
                            recipe_id=post[2],
                            comment_text=post[3],
                        )
                        result.append(comment)
                    return result
        except Exception as e:
            print(e)
            return {"message": "Could not list comments"}

    def getreviewcomments(self) -> Union[Error, List[CommentReviewOut]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT comment.id, comment.user_id,
                        comment.review_id, comment.comment_text
                        FROM comment
                        INNER JOIN review
                            ON (comment.review_id = review.id)
                        ORDER BY review_id DESC
                        """
                    )
                    result = []
                    for post in db:
                        comment = CommentReviewOut(
                            id=post[0],
                            user_id=post[1],
                            review_id=post[2],
                            comment_text=post[3],
                        )
                        result.append(comment)
                    return result
        except Exception as e:
            print(e)
            return {"message": "Could not list comments"}

    def artcomment(self, comment: CommentIn):
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                        INSERT INTO comment
                            (user_id, art_id, comment_text)
                        VALUES
                            (%s, %s, %s)
                        RETURNING id;
                        """,
                    [comment.user_id, comment.art_id, comment.comment_text],
                )
                id = result.fetchone()[0]
                old_data = comment.dict()
                return CommentArtOut(id=id, **old_data)

    def recipecomment(self, comment: CommentIn):
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                        INSERT INTO comment
                            (user_id, recipe_id, comment_text)
                        VALUES
                            (%s, %s, %s)
                        RETURNING id;
                        """,
                    [comment.user_id, comment.recipe_id, comment.comment_text],
                )
                id = result.fetchone()[0]
                old_data = comment.dict()
                return CommentRecipeOut(id=id, **old_data)

    def reviewcomment(self, comment: CommentIn):
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                        INSERT INTO comment
                            (user_id, review_id, comment_text)
                        VALUES
                            (%s, %s, %s)
                        RETURNING id;
                        """,
                    [comment.user_id, comment.review_id, comment.comment_text],
                )
                id = result.fetchone()[0]
                old_data = comment.dict()
                return CommentReviewOut(id=id, **old_data)
