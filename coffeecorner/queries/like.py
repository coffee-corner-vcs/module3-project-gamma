from pydantic import BaseModel
from queries.pool import pool
from typing import List, Optional, Union


# from typing import Optional


class Error(BaseModel):
    message: str


class LikeIn(BaseModel):
    user_id: int
    art_id: Optional[int]
    review_id: Optional[int]
    recipe_id: Optional[int]


class LikeArtOut(BaseModel):
    id: int
    user_id: int
    art_id: int


class LikeRecipeOut(BaseModel):
    id: int
    user_id: int
    recipe_id: int


class LikeReviewOut(BaseModel):
    id: int
    user_id: int
    review_id: int


class LikeRepository:
    def deleteartlike(self, id: int) -> Optional[LikeArtOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM likes
                        WHERE id = %s
                        """,
                        [id],
                    )
                    return True
        except Exception as e:
            print(e)
            return False

    def deleterecipelike(self, id: int) -> Optional[LikeRecipeOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM likes
                        WHERE id = %s
                        """,
                        [id],
                    )
                    return True
        except Exception as e:
            print(e)
            return False

    def deletereviewlike(self, id: int) -> Optional[LikeReviewOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM likes
                        WHERE id = %s
                        """,
                        [id],
                    )
                    return True
        except Exception as e:
            print(e)
            return False

    def getartlikes(self, account_id) -> Union[Error, List[LikeArtOut]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT likes.id, likes.user_id, likes.art_id,
                        art.image, art.alt_text, art.description, account.image
                        FROM likes
                        INNER JOIN art
                            ON (likes.art_id = art.id)
                        INNER JOIN account
                            ON (likes.user_id = account.id)
                        ORDER BY art_id DESC
                        """
                    )
                    result = []
                    for post in db:
                        if post[1] == account_id["id"]:
                            artpost = {
                                "id": post[0],
                                "user_id": post[1],
                                "art_id": post[2],
                                "image": post[3],
                                "alt_text": post[4],
                                "description": post[5],
                                "profile_image": post[6],
                            }
                            result.append(artpost)
                    return result
        except Exception as e:
            print(e)
            return {"message": "Could not list likes"}

    def getrecipelikes(self, account_id) -> Union[Error, List[LikeRecipeOut]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT likes.id, likes.
                        user_id, likes.recipe_id, recipe.title,
                        recipe.image, recipe.alt_text,
                        recipe.description, recipe.ingredients,
                        recipe.roast, account.image
                        FROM likes
                        INNER JOIN recipe
                            ON (likes.recipe_id = recipe.id)
                        INNER JOIN account
                            ON (likes.user_id = account.id)
                        ORDER BY recipe_id DESC
                        """
                    )
                    result = []
                    for post in db:
                        if post[1] == account_id["id"]:
                            recipepost = {
                                "id": post[0],
                                "user_id": post[1],
                                "recipe_id": post[2],
                                "title": post[3],
                                "image": post[4],
                                "alt_text": post[5],
                                "description": post[6],
                                "ingredients": post[7],
                                "roast": post[8],
                                "profile_image": post[9],
                            }
                            result.append(recipepost)
                    return result
        except Exception as e:
            print(e)
            return {"message": "Could not list likes"}

    def getreviewlikes(self, account_id) -> Union[Error, List[LikeReviewOut]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT likes.id, likes.user_id,
                        likes.review_id, review.image,
                        review.alt_text, review.description,
                        review.rating, review.date_created, account.image
                        FROM likes
                        INNER JOIN review
                            ON (likes.review_id = review.id)
                        INNER JOIN account
                            ON (likes.user_id = account.id)
                        ORDER BY review_id DESC
                        """
                    )
                    result = []
                    for post in db:
                        if post[1] == account_id["id"]:
                            reviewpost = {
                                "id": post[0],
                                "user_id": post[1],
                                "review_id": post[2],
                                "image": post[3],
                                "alt_text": post[4],
                                "description": post[5],
                                "rating": post[6],
                                "date_created": post[7],
                                "profile_image": post[8],
                            }
                            result.append(reviewpost)
                    return result
        except Exception as e:
            print(e)
            return {"message": "Could not list likes"}

    def artlike(self, like: LikeIn):
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    INSERT INTO likes
                        (user_id, art_id)
                    VALUES
                        (%s, %s)
                    RETURNING id;
                    """,
                    [like.user_id, like.art_id],
                )
                id = result.fetchone()[0]
                old_data = like.dict()
                return LikeArtOut(id=id, **old_data)

    def recipelike(self, like: LikeIn):
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    INSERT INTO likes
                        (user_id, recipe_id)
                    VALUES
                        (%s, %s)
                    RETURNING id;
                    """,
                    [like.user_id, like.recipe_id],
                )
                id = result.fetchone()[0]
                old_data = like.dict()
                return LikeRecipeOut(id=id, **old_data)

    def reviewlike(self, like: LikeIn):
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    INSERT INTO likes
                        (user_id, review_id)
                    VALUES
                        (%s, %s)
                    RETURNING id;
                    """,
                    [like.user_id, like.review_id],
                )
                id = result.fetchone()[0]
                old_data = like.dict()
                return LikeReviewOut(id=id, **old_data)
