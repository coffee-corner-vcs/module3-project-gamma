steps = [
    [
    """
    ALTER TABLE recipe

    ADD COLUMN
    user_id INT REFERENCES account(id) ON DELETE CASCADE NOT NULL;
    """,
    """
    DROP COLUMN user_id;
    """,
    ],
      [
    """
    ALTER TABLE review

    ADD COLUMN
    user_id INT REFERENCES account(id) ON DELETE CASCADE NOT NULL;
    """,
    """
    DROP COLUMN user_id;
    """,
    ],
      [
    """
    ALTER TABLE art

    ADD COLUMN
    user_id INT REFERENCES account(id) ON DELETE CASCADE NOT NULL;
    """,
    """
    DROP COLUMN user_id;
    """,
    ],
]
