from queries.reviews import ReviewRepository
from fastapi.testclient import TestClient
from main import app
from authenticator import authenticator


client = TestClient(app)


class FakeReviewRepository:
    def get_all(self):
        result = [
            {
                "id": 1,
                "image": "https://tinyurl.com/dsfdfedfs",
                "alt_text": "alt text",
                "description": "Description for cafe",
                "rating": "5",
                "date_created": "2023-06-07T00:00:00",
                "user_id": 1,
            },
            {
                "id": 2,
                "image": "https://tinyurl.com/dsfdfedfs",
                "alt_text": "alt text 2",
                "description": "Description for second cafe",
                "rating": "4",
                "date_created": "2023-03-30T00:00:00",
                "user_id": 1,
            },
        ]

        return result


def fake_get_current_account_data():
    return {"user_id": 1}


def test_get_reviews():
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data
    app.dependency_overrides[ReviewRepository] = FakeReviewRepository

    response = client.get("/reviews")

    app.dependency_overrides = {}

    assert response.status_code == 200
    assert response.json() == [
        {
            "id": 1,
            "image": "https://tinyurl.com/dsfdfedfs",
            "alt_text": "alt text",
            "description": "Description for cafe",
            "rating": "5",
            "date_created": "2023-06-07T00:00:00",
            "user_id": 1,
        },
        {
            "id": 2,
            "image": "https://tinyurl.com/dsfdfedfs",
            "alt_text": "alt text 2",
            "description": "Description for second cafe",
            "rating": "4",
            "date_created": "2023-03-30T00:00:00",
            "user_id": 1,
        },
    ]
